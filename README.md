# test-rebelworks

### TEORI
1. Jelaskan modifires private, protected, dan public? 
- private adalah modifier dimana hanya bisa diakses didalam class tersebut.
- protected adalah modifier dimana hanya bisa diakses didalam class itu sendiri, sub class atau inheritance dan package yang berbeda.
- public adalah modifier yang bisa diakses dari class manapun.


2. Jelaskan modifires final dan static?
- final adalah salah satu modifier yang memberikan sifat final value (tidak dapat diubah) pada Property dan Method.
- static adalah salah satu modifier yang memungkinkan mengakses Property atau Method dari Class lain tanpa harus menginisialisasi atau instance Class tersebut terlebih dahulu.

3. Jelaskan class inheritance?
adalah konsep dimana sebuah class dapat menurunkan property dan method yang dimilikinya kepada class lain.

4. Jelaskan design pattern faced dan factory?
- design patern faced adalah pattern yang menyembunyikan design pattern faced. Dimana class tersebut nanti akan diakses oleh client. Sehingga client dimudahkan untuk menggunakan sistem yang dibuat.
- design pattern factory adalah kerangka dari obyek yang akan dibuat dimana berbentuk abstract class. lalu product dibuat melalui Concrete Factory.


### Logic Test
1.  - 20 mobil
    - setiap balapan hanya 5 mobil saja
    - berapa banyak balapan utk mengetahui 3 mobil tercepat

    - 20 / 5 = 4 balapan

    - balapan 1 = 5 mobil dan akan diambil 1 mobil terbaik => 1
    - balapan 2 = 5 mobil dan akan diambil 1 mobil terbaik => 1
    - balapan 3 = 5 mobil dan akan diambil 1 mobil terbaik => 1
    - balapan 4 = 5 mobil dan akan diambil 1 mobil terbaik => 1
    - balapan 5 = diikuti 4 (dimana diambil 1 dari setiap balapan, dari balapan 1 - 4) mobil dan akan diambil 3 mobil terbaik

    - jadi utk menentukan 3 mobil tercepat dari 20 mobil adalah dengan 5 balapan

2.  - Jawaban pada (a) Orangtua dari node 3135 = 1045 || i = 3135 / 3 = 1045
    - Jawaban pada (b) 
        - untuk mencari a / left node adalah dengan a = (i * 3) - 1
        - untuk mencari b / inner node adalah dengan b = (i * 3)
        - untuk mencari c / right node adalah dengan c = (i * 3) + 1
        
        ```js
        function searchNodeInner(num) {
          let i = num / 3;
          return i;
        }

        console.log(searchNodeInner(3135)); 
        ```

3. Jawaban No 3
```js
function hitungJumlahTunjanganAnak(gajiPokok, totalAnak, arrAnak) {
    let tunjangan = 0;
    let sum = 0;

    for (let i = 0; i < totalAnak; i++) {
        if (arrAnak[i] >= 1 && arrAnak[i] <= 5) {
            tunjangan = gajiPokok * 0.05;
        } 

        if (arrAnak[i] >= 6 && arrAnak[i] <= 10) {
            tunjangan = gajiPokok * 0.07;
        }

        if (arrAnak[i] >= 11 && arrAnak[i] <= 15) {
            tunjangan = gajiPokok * 0.1;
        }

        sum += tunjangan;
    }

    return "Jumlah tunjangan anak = " + sum;
}


// input gaji pokok
// input total anak
// input masing - masing umur anak => array

let gajiPokok = 1000000;
let totalAnak = 2;
let arrAnak = [10, 5];

console.log(hitungJumlahTunjanganAnak(gajiPokok, totalAnak, arrAnak));
```
